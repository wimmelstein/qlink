package nl.wimmelstein.qlink.controller;

import nl.wimmelstein.qlink.domain.Link;
import nl.wimmelstein.qlink.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;


@RestController
@RequestMapping("/qlink")
public class qlinkController {

    @Autowired
    private LinkRepository linkRepository;

    @RequestMapping(value = "retrieve", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    Link link(
            @RequestParam(value = "keyword") String keyword) {

        Link responseLink = linkRepository.findByKeywordIgnoreCase(keyword);
        if (responseLink != null) {
            return responseLink;
        } else
            return new Link("error", "#", "Sorry, your keyword was not found", LocalDate.now());
    }


}