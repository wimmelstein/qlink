package nl.wimmelstein.qlink.repository;

import nl.wimmelstein.qlink.domain.Link;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface LinkRepository extends JpaRepository<Link, String> {

    Link findByKeywordIgnoreCase(String keyword);


}
