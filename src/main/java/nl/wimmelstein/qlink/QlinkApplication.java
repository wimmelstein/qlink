package nl.wimmelstein.qlink;

import nl.wimmelstein.qlink.domain.Link;
import nl.wimmelstein.qlink.repository.LinkRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;


@SpringBootApplication
public class QlinkApplication {

    public static void main(String[] args) {

		SpringApplication.run(QlinkApplication.class, args);

	}

	@Bean
	CommandLineRunner runner (LinkRepository linkRepository) {
		return args -> {
            
            Stream.of("aap;http://www.apenheul.nl;Een dageje naar de apen,advocaat;http://www.bakertillyberk.nl;Goede advocaten".split(","))
                    .forEach(t -> linkRepository.save(new Link(t)));
                linkRepository.findAll().forEach(System.out::println);
		};
	}
}
