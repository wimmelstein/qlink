package nl.wimmelstein.qlink.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Link {
    public Link(String keyword, String url, String description, LocalDate expires) {

        this.keyword = keyword;
        this.url = url;
        this.description = description;
        this.expires = expires;
    }

    Link() {

    }

    public Link(String link) {

        this(link.split(";")[0], link.split(";")[1], link.split(";")[2], LocalDate.now().plusMonths(2));
    }

    @Id
    @GenericGenerator(name = "uuid-gen", strategy = "uuid")
    @GeneratedValue(generator = "uuid-gen")
    private String id;
    private String keyword;
    private String url;
    private String description;
    LocalDate expires;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getExpires() {
        return expires;
    }

    public void setExpires(LocalDate expires) {
        this.expires = expires;
    }

    @Override
    public String toString() {
        return "Link{" +
                "id=" + id +
                ", keyword='" + keyword + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", expires=" + expires +
                '}';
    }
}
