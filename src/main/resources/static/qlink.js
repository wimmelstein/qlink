$('#qlink-form').submit( function ( event) {

    var data = [];
    var link;
    event.preventDefault();

    $.ajax({
        url: "qlink/retrieve?keyword=" + $("#keyword").val(),
        data: data,
        success: function (data) {
            if(data.url != "#") {
                link = '<a href="' + data.url + '">' + data.description + '</a>';
            } else {
                link = "Sorry, your keyword was not found";
            }
            $( '.results').html(link);
        }
    })
})
